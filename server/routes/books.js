/*jslint node:true */
/** TODO: Test with static-analyzer */


/** @module Routes for books */
/** @class */
/** TODO: Define module */
var express = require('express');
var router = express.Router();

    /**  book routes
     ---------------
     We create a variable "user" that holds the controller object.
     We map the URL to a method in the created variable "controller".
     In this example is a mapping for every CRUD action.
     */
/** TODO: Define variable(s) */
var controller = require('../app/controllers/books.js');

    // CREATE
    /** CREATE route for books */
    /** TODO: Define route for CREATE 1 document */
router.post('/books', controller.create);
    // RETRIEVE
    /** TODO: Define route for RETRIEVE all documents */
    /** TODO: Define route for RETRIEVE 1 document */
router.get('/books', controller.list);

router.get('/books/:_id', controller.detail);
    // UPDATE
    /** TODO: Define route for UPDATE 1 document */
router.put('/books/:_id', controller.updateOne);
    // DELETE
    /** TODO: Define route for DELETE 1 document */
router.delete('/books/:_id', controller.deleteOne);

module.exports = router;