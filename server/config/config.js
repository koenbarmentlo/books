/*jslint node:true */
"use strict";
/** TODO: Test with static-analyzer */

module.exports = {
    development: {
        db: 'mongodb://server3.tezzt.nl/S499793books-dev',    // change books with your database
        port: 9021,                             // change 3000 with your port number
        debug: true                             // set debug to true|false
    },
    test: {
        db: 'mongodb://server3.tezzt.nl/S499793books-dev',   // change books with your database
        port: 8080,                             // change 1300 with your port number
        debug: false                            // set debug to true|false
    },
    production: {}
};