/*jslint node: true*/
"use strict";

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    modelName,
    schemaName;

/**
 * Creates a new mongoose schema.
 * -----
 * Instructions, hints and questions
 * - Instruction: create a Schema with the following properties
 *   - title
 *   - author
 *   - description
 *   - modificationDate
 * - The properties are defined as follow:
 *   - title, author and description are strings
 *   - modificationDate is a date
 *   - a title of the book is unique
 *   - title and author are required
 *   - the default value for date is now
 *
 * - Question: What are the differences between a 'Schema Type' and a JSON object? Use the references to motivate your answer.
 * - Answer: The Schema Type defines what the JSON object must look like including property names and datatypes. The JSON object is an actual object or array in serialized to text.
 * - Question: What is the function of the 'collection' property?
 * - Answer: It's the name of the MongoDB Collection. The CRUD operations are performed on this collection.
 * - Question: Suppose you have a model for 'Person'. What would be the default collection name?
 * - Answer: Persons
 * @class Schema/Book
 * @returns Schema object
 * @see http://www.json.org/
 * @see http://mongoosejs.com/docs/schematypes.html
 * @see http://mongoosejs.com/docs/guide.html#collection
 */
schemaName = new Schema(
    {
        title: { type: String, required: true, unique: true },
        author: { type: String, required: true },
        description: { type: String },
        modificationDate: { type: Date, "default": Date.now }
    },
    { collection: 'books' }
);

/**
 * Custom validator
 * -------
 * Instructions, hints and questions.
 *
 * In Mongoose you can define custom validators.
 * If the value does not fit the the definition, an error is returned.

 * - Instruction: Add a validator for title. A title must have a length of at least 8 characters.
 * - Question: There are four locations of validations, each for a specific type of validation. Give for each validation an example and describe why that location is the best location for that specific type of validation.
 *   1. Database (technical constraints, primary key)
 *   To prevent database pollution.
 *   1. Schema (simple business rules)
 *   The schema is a definition of what an object looks like. This is the best place to define data types and property names.
 *   1. Application (complex business rules)
 *   In the application you can place code. With that code you can implement complex business rules.
 *   1. Client-side (form validation)
 *   So you don't have to send an extra request to the server for validation.
 * @class Validator/Book/title
 * @returns true or false. In case of ```false```, a message 'Invalid title' is returned as well.
 * @see http://mongoosejs.com/docs/validation.html
 */
schemaName.path('title').validate(function (val) {
    return (val !== undefined && val !== null && val.length >= 8);
}, 'Invalid title');

/**
 * Instructions, hints and questions.
 * - Instruction: Create a model for the defined schema.
 * - Question: What are the differences between a 'Model' and a 'Schema Type'? Use the references to motivate your answer.
 * A schema represents the structure of a document or a part of the document. A model is the actual document. The property names and types defined in the schema, have real values in te model.
 * @class Model/Book
 * @see http://mongoosejs.com/docs/models.html
 */
modelName = "Book";
module.exports = mongoose.model(modelName, schemaName);