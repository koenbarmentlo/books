/*jslint node: true*/
"use strict";
/** TODO: Test with static-analyzer */

/**
 * TODO: Define variables
 */
var mongoose = require('mongoose'),
    Book = mongoose.model('Book');

/**
 * CREATE a book
 * --------------------
 * Controller to create a book.
 *
 * Instructions, hints and questions
 * - Read about the 'save' method from Mongoose.
 * - Use the 'save' method from Mongoose.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   Answer: MongoDb is database system (like MS SQL Server, Postgres or Cassandra). Mongoose is an object-relational mapping framework. It makes CRUD functionality on the database easier. It's also used for implementing business logic and data validation etc.
 *   - Question: explain the concepts of 'schema type' and 'model'. How are they related?
 *   Answer: A schema represents the structure of a document or a part of the document. A model is the actual document. The property names and types defined in the schema, have real values in te model.
 * - Return all fields.
 * - Use the model "Book".
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object, in case of retrieving all objects, this is always an array. No documents is returned as an empty array.
 * - err: If no errors, it has the value 'null'
 *
 * Errors are not thrown in the node application but returned to the user.
 * - Question: What will happen if you throw an error on the server?
 * Answer: The server will stop. If the server returns the error, the error will be send to the client and the server will keep running.
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.save/
 * @see http://mongoosejs.com/docs/api.html#model_Model-save
 * @module books/create
 */
/**
 * TODO: Create a CREATE document controller
 */
exports.create = function (req, res) {
    var doc = new Book(req.body);
    doc.save(function (err) {
        var retObj = {
            meta: {
                "action": "create",
                'timestamp': new Date(),
                filename: __filename
            },
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};

/**
 * RETRIEVE _all_ books
 * --------------------
 * Controller to retrieve _all_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'find' method from Mongoose.
 * - Use the 'save' method from Mongoose.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   Answer: MongoDb is database system (like MS SQL Server, Postgres or Cassandra). Mongoose is an object-relational mapping framework. It makes CRUD functionality on the database easier. It's also used for implementing business logic and data validation etc.
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 *   Answer: Because all the documents must be retrieved. With a query, a part of the records in a collection will be returned.
 * - Skip the options.
 *   - Question: Describe the options.
 *   Answer: There are no options in this request.
 * - Return all fields.
 * - Use the model "Book".
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object, in case of retrieving all objects, this is always an array. No documents is returned as an empty array.
 * - err: If no errors, it has the value 'null'
 *
 * Errors are not thrown in the node application but returned to the user.
 * - Question: What will happen if you throw an error on the server?
 * Answer: The server will stop. If the server returns the error, the error will be send to the client and the server will keep running.
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.find/
 * @see http://mongoosejs.com/docs/api.html#model_Model.find
 * @module books/list
 */
/**
 * TODO: Create a RETRIEVE all document controller
 */
exports.list = function (req, res) {
    var conditions, fields, sort;
    conditions = {};
    fields = {};
    sort = { 'modificationDate': -1 };
    Book.find(conditions, fields)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: { "action": "list", "timestamp": new Date(), filename: __filename },
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * RETRIEVE _one_ book
 * --------------------
 * Controller to retrieve _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'findOne' method from Mongoose.
 * - Use the 'findOne' method from Mongoose.
 *   - Question: What is de result object from findOne?
 *   Answer: Book
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   Answer: MongoDb is database system (like MS SQL Server, Postgres or Cassandra). Mongoose is an object-relational mapping framework. It makes CRUD functionality on the database easier. It's also used for implementing business logic and data validation etc.
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 *   Answer: It's not, it's called conditions.
 * - Skip the options.
 * - Return all fields.
 * - Use the model "Book".
 * Question: Define route parameters and body parameter. What are the differences?
 * Answer: Route parameters are extra information for the page: https://www.google.nl/?q=hallo (q is the parameter name and hallo is the value)
 *         Body parameters define the URL (route). http://localhost:8080/books/horror (books and horror are the route parameters)
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/detail
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.findOne/
 * @see http://mongoosejs.com/docs/api.html#model_Model.findOne
 */
/**
 * TODO: Create a RETRIEVE 1 document controller
 */
exports.detail = function (req, res) {
    var conditions, fields;
    conditions = { _id: req.params._id };
    fields = {};

    Book.findOne(conditions, fields)
        .exec(function (err, doc) {
            var retObj = {
                meta: { "action": "detail", "timestamp": new Date(), filename: __filename },
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * UPDATE book
 * --------------------
 * Controller to update _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'find' method from Mongoose.
 * - Use the 'findOneAndUpdate' method from Mongoose.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   Answer: MongoDb is database system (like MS SQL Server, Postgres or Cassandra). Mongoose is an object-relational mapping framework. It makes CRUD functionality on the database easier. It's also used for implementing business logic and data validation etc.
 *   - Question: What are the differences between MongoDb 'save' and MongoDb 'update'?
 *   Save updates a document if it exists and if it not exists the document will be inserted.
 *   With update you cannot create a new document. You can change specific fields of an existing document or replace an document entirely.
 * - Return all fields.
 * - Use the model "Book".
 * Question: What changes should be made to update more than one document?
 * Answer: The conditions must be changed. It now updates a document specified by it's ID and the ID's are unique.
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/update
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.update/
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.save/
 * @see http://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
 */
 /**
  * TODO: Create a UPDATE document controller
  */
exports.updateOne = function (req, res) {
    var conditions =
        { _id: req.params._id },
        update = {
            title: req.body.title || '',
            author: req.body.author || '',
            description: req.body.description || ''
        },
        options = { multi: false },
        callback = function (err, doc) {
            var retObj = {
                meta: { "action": "update", "timestamp": new Date(), filename: __filename },
                doc: doc,
                err: err
            };
            return res.send(retObj);
        };
    Book.findOneAndUpdate(conditions, update, options, callback);
};

/**
 * DELETE
 * remove @ http://mongoosejs.com/docs/api.html#model_Model-remove
 * @param req
 * @param res
 */

/**
 * DELETE _one_ book
 * --------------------
 * Controller to delete _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'findOne' method from Mongoose.
 * - Use the 'findOne' method from Mongoose.
 *   - Question: What is de result object from findOne?
 *   Answer: Book
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   Answer: MongoDb is database system (like MS SQL Server, Postgres or Cassandra). Mongoose is an object-relational mapping framework. It makes CRUD functionality on the database easier. It's also used for implementing business logic and data validation etc.
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 *   Answer: It's not. The query is called conditions in this function.
 * - Skip the options.
 * - Return all fields.
 * - Use the model "Book".
 * Question: Define route parameters and body parameter. What are the differences?
 * Answer: Route parameters are extra information for the page: https://www.google.nl/?q=hallo (q is the parameter name and hallo is the value)
 *         Body parameters define the URL (route). http://localhost:8080/books/horror (books and horror are the route parameters)
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/detail
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.remove/
 * @see http://mongoosejs.com/docs/api.html#model_Model.remove
 */
/**
 * TODO: Create a DELETE document controller
 */
exports.deleteOne = function (req, res) {
    var conditions, callback, retObj;
    console.log('Deleting book. ', req.params._id);
    conditions = { _id: req.params._id };
    callback = function (err, doc) {
        retObj = {
            meta: { "action": "delete", "timestamp": new Date(), filename: __filename },
            doc: doc,
            err: err
        };
        return res.send(retObj);
    };
    Book.remove(conditions, callback);
};