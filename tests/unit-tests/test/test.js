// Load configuration

/** TODO: Load configurations */
var env = process.env.NODE_ENV || 'development',
    config = require('../../../server/config/config.js')[env],
    localConfig = require('./../../config-test.json');
/** TODO: Define variables */
var should = require('should'),
    supertest = require('supertest');

/** TODO: Create test set */
describe('API Routing for CRUD operations on books', function() {
    console.log("Request: " + localConfig.host + ":" + config.port + "/" + localConfig.api_path + "/");
    var request = supertest(localConfig.host + ":" + config.port + "/" + localConfig.api_path + "/");
    var tmpBookId = null;
    var tmpBookResponse;
    before(function (done) {
        done();
    });

    /** TODO: Create http calls to test API's with supertest */
    describe('CREATE book', function () {
        it('Should POST /books', function (done) {
            request
                .post('/books')
                .send({
                    "title": "Nice book!" + Date.now(),
                    "author": "John Doe",
                    "description": "hello world!"
                })
                .expect(200)
                .expect('Content-Type', /application.json/)
                .expect('Content-Type', 'utf-8')
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('create');
                    JSON.parse(res.text)
                        .should.have.property('err').be.exactly(null);
                    res.statusCode.should.be.exactly(200);
                    res.type.should.be.exactly('application/json')
                    res.charset.should.be.exactly('utf-8');
                    tmpBookId = JSON.parse(res.text).doc._id;
                    done();
                });
        });
    });

    /** TODO: Create test case for CREATE, RETRIEVE all, RETRIEVE 1, UPDATE, DELETE  */
    describe('Retrieve 1 book', function() {
       it('Should GET /books/{id}', function(done) {
           request
               .get('/books/' + tmpBookId)
               .expect('Content-Type', /application.json/)
               .expect(200)
               .end(function(err, res) {
                   if(err) {
                       throw err;
                   }
                   JSON.parse(res.text)
                       .should.have.property('meta')
                       .and.have.property('action').be.exactly('detail');
                   res.statusCode.should.be.exactly(200);
                   done();
               });
       });
    });

    describe('UPDATE 1 book', function() {
       it('Should put /books/{id}', function(done) {
           request
               .put('/books/' + tmpBookId)
               .send({
                  "doc": {
                      "title": "Really nice book " + Date.now(),
                      "author": "Koen Barmentlo",
                      "description": "updated"
                  }
               })
               .expect(200)
               .expect('Content-Type', /application.json/)
               .expect('Content-Type', 'utf-8')
               .end(function(err, res) {
                   if(err) {
                       throw err;
                   }
                   JSON.parse(res.text)
                       .should.have.property('meta')
                       .and.have.property('action').be.exactly('update');
                   JSON.parse(res.text)
                       .should.have.property('err').be.exactly(null);
                   res.statusCode.should.be.exactly(200);
                   done();
               });
       });
    });

    describe('DELETE 1 book', function() {
       it('Should delete /books/{id}', function(done) {
           request
               .del('/books/' + tmpBookId)
               .expect(200)
               .expect('Content-Type', /application.json/)
               .expect('Content-Type', 'utf-8')
               .end(function(err, res) {
                   if(err) {
                       throw err;
                   }
                   JSON.parse(res.text)
                       .should.have.property('meta')
                       .and.have.property('action').be.exactly('delete');
                   JSON.parse(res.text).should.have.property('doc').be.exactly(1)
                   JSON.parse(res.text).should.have.property('err').be.exactly(null)
                   res.statusCode.should.be.exactly(200);
                   done();
               });
       });
    });

    describe('Retrieve all books to verify that the original collection is restored', function(done) {
        it('Should GET /books', function(done) {
            request
                .get('/books')
                .expect(201)
                .expect('Content-Type', /application.json/)
                .expect('Content-Type', 'utf-8')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('list');
                    res.statusCode.should.be.exactly(200);
                    done();
                });
        });
    });

    /** TODO: Create assertions with supertest */
    /** TODO: Create assertions with should */

});

