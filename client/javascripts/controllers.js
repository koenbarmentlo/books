/*jslint node: true */
/*globals myApp */


/**
 * TODO: create controller for book list
 * @param $scope
 * @param booksService
 * @constructor
 */
/** TODO: Create functionality to retrieve all books */
function BookListCtrl($scope, booksService) {
    "use strict";
    $scope.books = booksService.books.get();
}

/**
 * TODO: create controller for retrieving 1 book, create and delete
 * @param $scope
 * @param $routeParams
 * @param booksService
 * @constructor
 */
function BookDetailCtrl($scope, $routeParams, $location, booksService) {
    "use strict";
    if ($routeParams.id !== '0') {
        $scope.books = booksService.books.get({_id: $routeParams._id}, function () {
            console.log('$scope.requests: ', $scope.requests);
        });
    }

    // DELETE book
    /** TODO: Create route to delete a book */
    $scope.delete = function () {
        booksService.books.delete({_id: $routeParams._id});
        $location.path("/books");
    };

    // CREATE, UPDATE book
    /** TODO: Create code to create, update a book */
    $scope.save = function () {
        if ($scope.books.doc && $scope.books.doc._id !== undefined) {
            console.log('entering update');
            booksService.books.update({_id: $scope.books.doc._id}, $scope.books.doc, function (res) {
                console.log(res);
            });
        } else {
            console.log('entering save');
            booksService.books.save({}, $scope.books.doc, function (res) {
                console.log(res);
            });
        }
    };
}

myApp.controller('myCtrl', function ($scope) {
    "use strict";
    $scope.whoami = "Koen Barmentlo";
});